import configparser

config = configparser.ConfigParser()
config.read("bot.ini")

LIKE_TYPE = int(config["DEFAULT"]["LIKE_TYPE"])
NUMBER_OF_USERS = int(config["DEFAULT"]["NUMBER_OF_USERS"])
MAX_POSTS_PER_USER = int(config["DEFAULT"]["MAX_POSTS_PER_USER"])
REGISTRATION_URL = config["DEFAULT"]["REGISTRATION_URL"]
GET_TOKEN_URL = config["DEFAULT"]["GET_TOKEN_URL"]
POST_CREATE = config["DEFAULT"]["POST_CREATE"]
POST_LIKE = config["DEFAULT"]["POST_LIKE"]