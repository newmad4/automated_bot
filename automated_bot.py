import random

from faker import Faker
import requests
import threading
import time

from config import *


class UserBehavior:
    posts_id = []
    users_tokens = []

    def __init__(self, token=None, new_user=True):
        fake = Faker()
        if new_user:
            self.password = fake.password()
            self.email = fake.email()
        self.token = token

    def registration(self, url=REGISTRATION_URL):
        payload = {'email': self.email, 'password': self.password}
        requests.post(url, payload)

    def get_token(self, url=GET_TOKEN_URL):
        payload = {'email': self.email, 'password': self.password}
        response = requests.post(url, payload)
        self.token = response.json().get('access')
        self.users_tokens.append(self.token)

    def create_post(self, url=POST_CREATE):
        if self.token:
            for _ in range(random.randint(1, MAX_POSTS_PER_USER)):
                fake = Faker()
                payload = {'title': fake.text(), 'text': fake.text()}
                response = requests.post(url, payload, headers={'Authorization': 'JWT ' + self.token})
                post_id = response.json().get('id')
                if post_id:
                    self.posts_id.append(post_id)

    def create_posts_by_auth_user(self):
        self.registration()
        self.get_token()
        self.create_post()

    def create_like(self, posts_id, like_type, url=POST_LIKE):
        if self.token and posts_id:
            for post_id in posts_id:
                payload = {'post': post_id, 'like_type': like_type}
                requests.post(url, payload, headers={'Authorization': 'JWT ' + self.token})


if __name__ == '__main__':
    print("Start")
    start_time = time.time()
    workers = []

    # start concurrent user threads
    for i in range(NUMBER_OF_USERS):
        user_behavior = UserBehavior()
        thread = threading.Thread(target=user_behavior.create_posts_by_auth_user, daemon=True)
        thread.start()
        workers.append(thread)

    # Block until all threads fi№ish.
    for w in workers:
        w.join()

    for token in UserBehavior.users_tokens:
        user_behavior = UserBehavior(token=token, new_user=False)
        thread = threading.Thread(
            target=user_behavior.create_like,
            kwargs={"posts_id": UserBehavior.posts_id, "like_type": LIKE_TYPE},
            daemon=True
        )
        thread.start()
        workers.append(thread)

        # Block until all threads finish.
        for w in workers:
            w.join()
    print("Finish")
    print("--- %s seconds ---" % (time.time() - start_time))
